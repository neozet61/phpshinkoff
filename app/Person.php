<?php

namespace App;

class Person
{
    public string $firstName;

    public string $secondName;

    public int $age;

    public string $country;

    public string $city;

    public string $phoneNumber;

    public string $seniority;

    /**
     * @param string $firstName
     * @param string $secondName
     * @param int $age
     * @param string $country
     * @param string $city
     * @param string $phoneNumber
     * @param string $seniority
     */
    public function __construct(string $firstName, string $secondName, int $age, string $country, string $city, string $phoneNumber, string $seniority)
    {
        $this->firstName = $firstName;
        $this->secondName = $secondName;
        $this->age = $age;
        $this->country = $country;
        $this->city = $city;
        $this->phoneNumber = $phoneNumber;
        $this->seniority = $seniority;
    }


}
