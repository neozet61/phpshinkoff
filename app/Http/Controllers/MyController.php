<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;


class MyController extends Controller
{
    public function actionForCalc(Request $request): string
    {
        if ($request->get('action') == "+") {
            $result = $request->get('float_a') + $request->get('float_b');

        } elseif ($request->get('action') == "-") {
            $result = $request->get('float_a') - $request->get('float_b');

        } elseif ($request->get('action') == "/") {
            $result = $request->get('float_a') / $request->get('float_b');

        } elseif ($request->get('action') == "*") {
            $result = $request->get('float_a') * $request->get('float_b');

        } else {
            echo $result = "Упс..";
        }
        return view('result', ["result"=>$result]);
    }

}


