<?php

namespace App\Http\Controllers;

use App\Person;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class CsvFileController
{
    public function show(Request $request)
    {
        // Парсим данные из файла csv
        $fileContent = file_get_contents('../data.csv');
        $rows = explode("\n", $fileContent);

        $headings = $rows[0];
        $headingsBase = explode(",", $headings);

        array_shift($rows);

        $persons = new Collection();

        foreach ($rows as $row) {
            $fields = explode(",", $row);

            if (count($fields) != 7) {
                continue;
            }

            $persons[] = new Person(
                $fields[0],
                $fields[1],
                $fields[2],
                $fields[3],
                $fields[4],
                $fields[5],
                $fields[6],
            );
        }

        $seniorityBase = $persons->pluck('seniority')->unique();

        $countryBase = $persons->pluck('country')->unique();

        // Получаем параметры фильтрации из GET запроса
        $firstNameFilter = $request->get('first_name');
        $secondNameFilter = $request->get('second_name');
        $age_from = $request->get('age_from');
        $age_to = $request->get('age_to');
        $seniority = $request->get('seniority');
        $country = $request->get('country');
        $way = $request->get('way');
        $filter_by = $request->get('filter_by');

        //используем $mapper для создания корректного запроса
        $mapper = [
            'First Name' => 'firstName',
            'Second Name' => 'secondName',
            'Age'=>'age',
            'Country'=>'country',
            'City'=>'city',
            'Phone Number'=>'phoneNumber',
            'Seniority'=>'seniority'
        ];


        // Фильтруем данные

        if (!is_null($firstNameFilter)) {
            $persons = $persons->filter(function ($person) use ($firstNameFilter) {
                return str_contains(mb_strtolower($person->firstName), $firstNameFilter);
            });
        }

        if (!is_null($secondNameFilter)) {
            $persons = $persons->filter(function ($person) use ($secondNameFilter) {
                return str_contains(mb_strtolower($person->secondName), $secondNameFilter);
            });
        }

        if (!is_null($age_from)) {
            $persons = $persons->where('age', '>=', $age_from);
        }

        if (!is_null($age_to)) {
            $persons = $persons->where('age', '<=', $age_to);
        }

        if (!is_null($seniority)) {
            $persons = $persons->where('seniority', '=', $seniority);
        }

        if (!is_null($country)) {
            $persons = $persons->where('country', '=', $country);
        }

        if(!is_null($way) and !is_null($filter_by)) {
            if ($way == "desc") {
                $persons = $persons->sortBy($mapper[$filter_by]);
            }

            if ($way == "asc") {
                $persons = $persons->sortByDesc($mapper[$filter_by]);
            }
        }

        return view('show_csv_file_controller', [
            'persons' => $persons,
            'seniorityBase' => $seniorityBase,
            'countryBase' => $countryBase,
            'headingsBase' => $headingsBase,
            'firstNameFilter' => $firstNameFilter,
            'secondNameFilter' => $secondNameFilter,
            'age_from' => $age_from,
            'age_to' => $age_to,
            'seniority' => $seniority
        ]);
    }

}
