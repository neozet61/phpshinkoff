<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/php', [\App\Http\Controllers\CsvFileController::class,"show"]);

/*Route::get('/', function () {
    return view('welcome');
});*/

/*Route::post('/result', [\App\Http\Controllers\MyController::class,"actionForCalc"]);*/

/*Route::post('/result', function (\Illuminate\Http\Request $request) {

    if ($request->get('action') == "+") {
        $result = $request->get('float_a') + $request->get('float_b');
        echo "Ваш результат $result";
    } elseif ($request->get('action') == "-" ) {
        $result = $request->get('float_a') - $request->get('float_b');
        echo "Ваш результат $result";
    } elseif ($request->get('action') == "/" ) {
        $result = $request->get('float_a') / $request->get('float_b');
        echo "Ваш результат $result";
    } elseif ($request->get('action') == "*" ) {
        $result = $request->get('float_a') * $request->get('float_b');
        echo "Ваш результат $result";
    } else {echo "Упс.."; }
});*/
